---
title: "Filamentos"
date: 2021-10-23T01:50:17-03:00
draft: false
---

### Os principais tipos de filamentos
#### PLA
O filamento PLA é um dos mais utilizados na impressão 3D pela facilidade de uso. É um material biodegradável, feito de fonte renovável como o amido de milho, cana de açúcar e raiz de tapioca. Não polui o meio ambiente e não emite gases que seja prejudiciais à saúde. Uma de suas vantagens é o baixo coeficiente de contração ao esfriar, desta forma a possibilidade de empenamentos, descolamento da mesa e de camadas é bem menor, por isso é indicado para produções de peças grandes. Esse material pode ser usado em diversas impressoras, pois ele não necessita de aquecimento da mesa para a impressão e também não necessita de altas temperaturas para a extrusão. Ele também é indicado para imprimir peças detalhadas, sem necessidade de acabamento posterior. É um plástico mais rígido, com isso é mais propenso a quebras por esforço mecânico e deformação pela proximidade de fontes de calor.

#### ABS
Junto com o PLA, o filamento ABS é também um material bastante utilizado. É um material derivado do petróleo e por isso há exaustão de gases durante a extrusão. Devido a exaustão de gases é aconselhável utilizá-lo em ambientes arejados. O ABS possui alta resistência e durabilidade. É recomendável que a mesa permaneça aquecida para garantir a impressão e para obter definições corretas é indicado para usuários mais avançados. Possui também coeficiente de contração muito alto, o que dificulta a impressão de peças grandes, correndo o risco de deformações e deslocamento da mesa durante a produção. É uma material sensível à exposição prolongada à luz solar, que pode degradas suas características ao longo do tempo.

#### HIPS
O filamento HIPS é um material solúvel em D-Limoneno. Amplamente utilizado como material de suporte primário em conjunto com outro extrusor, HIPS pode ser facilmente removido do seu objeto durante o pós-processamento. Em poucas horas, o suporte HIPS se dissolvera em solvente, deixando apenas o objeto impresso. Como a principal aplicação do HIPS é ser um suporte solúvel, esse material é mais indicado para impressoras com duplo extrusor.

#### PETG
O filamento PET é composto de politereftalato de etileno, este material é o mais utilizado no mundo, mais encontrado em garrafas de plástico. Para utilizá-lo em impressões na impressora 3D é utilizado o PETG, modificado com Glycol para ficar mais claro e facilitar o uso. Por ser um material muito resistente, o PETG é a escolha perfeita para quando é necessário imprimir peças que precisam absorver impactos. O filamento pode ser usado em impressoras abertas ou fechadas, com mesas aquecidas ou não, além de não emitir gases tóxicos nem rachar. 

#### Flexível
O filamento Flexível é baseado em TPU (Poliuretano Termoplástico). O filamento possui propriedades únicas de resistência à flexão, pois objetos impressos em 3D terão uma “memória de flexão”, permitindo que os objetos retornem à sua posição ou forma original depois de serem dobrados. Seus principais benefícios são a flexibilidade, resistência ao calor a longo prazo, excelente resistência UV e boa resistência a produtos químicos. Este tipo de filamento não é recomendável para a utilização em todas as impressoras e demandam certo tipo de cuidados quanto aos materiais e equipamentos.  Para imprimir com perfeição é necessário verificar sempre a sua máquina, pois a folga excessiva entre o tracionador e o extrusor pode fazer com que o filamento dobre e a impressão seja interrompida. 

![filamentorosa](https://www.cellshop.com/5596788-large_default/filamento-pla-para-impressora-3d-flashforge-175mm-1kg-vermelho.jpg) ![tabela](https://imagens.3dlab.com.br/wp-content/uploads/2019/12/guia_escolha_1-1.png) ![filamentoazul](https://images.tcdn.com.br/img/img_prod/650361/filamento_pla_para_impressao_3d_1_75mm_1kg_impressora_3d_cor_preta_1723_1_20190509095212.jpg)