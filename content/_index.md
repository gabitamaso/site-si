# O que é impressão 3D?

A impressão 3D, também chamada de fabricação aditiva (inglês), é uma família de processos que produz objetos ao adicionar material em camadas que correspondem a seções transversais sucessivas de um modelo 3D. O plástico e as ligas de metal são os materiais mais comumente usados para impressão 3D, mas quase tudo pode ser usado — de concreto a tecido vivo.



### CRIAÇÃO DE PROTÓTIPOS
![impressao](https://www.autodesk.com.br/content/dam/autodesk/www/solutions/3d-printing/prototyping-thumb-600x300.jpg) 

Há muito tempo a impressão 3D vem sendo usada para criar rapidamente protótipos, maquetes de montagem e modelos de apresentação.

### PEÇAS LEVES
![impressao](https://www.autodesk.com.br/content/dam/autodesk/www/solutions/3d-printing/lightweight-parts-thumb-600x300.jpg)

A eficiência dos combustíveis e a redução das emissões estão impulsionando a necessidade de peças leves criadas por meio da impressão 3D para aplicações aeroespaciais e automotivas. 

### PRODUTOS APRIMORADOS DE FORMA FUNCIONAL
![impressao](https://www.autodesk.com.br/content/dam/autodesk/www/solutions/3d-printing/functionally-enhanced-products-thumb-600x300.jpg)

A impressão 3D elimina muitas das restrições impostas pelos processos tradicionais de fabricação que impedem que os engenheiros projetem para alcançar um ótimo desempenho.

### IMPLANTES MÉDICOS PERSONALIZADOS
![impressao](https://www.autodesk.com.br/content/dam/autodesk/www/solutions/3d-printing/custom-medical-implants-thumb-600x300.jpg)

Para obter a osseointegração, os fabricantes estão usando a impressão 3D para controlar com precisão a porosidade da superfície a fim de reproduzir melhor a estrutura do osso real.
