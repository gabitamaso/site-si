---
title: "Funko POP"
date: 2021-09-18T23:27:57-03:00
draft: false
---

As aplicação da impressão 3D são inúmeras. Dentre elas, pode-se imprimir bonequinhos do estilo "Funko POP", alguns mais complexos com mais de uma cor, e outros monocromáticos. Alguns desses exemplos estão apresentados abaixo:


**La Casa de Papel** | **Matrix** | **Dementador**
:-------------------------:|:-------------------------:|:-------------------------:
![funko](https://img.elo7.com.br/product/zoom/364120F/funko-pop-la-casa-de-papel-impressao-3d.jpg) | ![matrix](https://img.elo7.com.br/product/main/377BEFA/funko-pop-tema-matrix-impressao-3d-flores-para-decoracao.jpg) | ![funko](https://img.elo7.com.br/product/zoom/2157F8C/dementador-de-biscuit-decoracao.jpg)

Além desses, é muito fácil de encontrar vários desses modelos na internet, disponíveis para fazer a sua própria impressão do *Funko POP* que você quiser.