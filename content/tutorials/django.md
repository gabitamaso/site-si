---
title: "Nylon"
date: 2021-09-18T23:27:57-03:00
draft: false
---
A impressão 3D geralmente utiliza dois tipos de plástico, ABS e PLA. Ambos são largamente documentados  e amplamente distribuídos. Mas este post documenta o processo aventurístico de imprimir com Nylon, ou Poliamida, justamente por não haver uma boa documentação sobre o assunto.

![filamentoazul](https://boaimpressao3d.com.br/wp-content/uploads/2015/04/wpid-wp-1428879502558-e1428927664473-232x300.jpeg)


### Atenção
Este material (tal como ABS/PLA) deve ser impresso em ambiente ventilado. Existem estudos que falam de uma possível toxidade do Nylon.

### Vantagens
Algumas vantagens observadas em imprimir em Nylon:

Flexibilidade;
Boa adesão entre camadas;
Custo;
Material muito forte;
Capacidade de ser tingido (ainda não testado);

### Qual Nylon?
Estamos falando de Nylon de roçadeira (!?)
Sim, um material feito para cortar grama que estamos testando a fim de torná-lo um material de impressão.
Existe uma marca de Nylon próprio para impressão que se chama Taulman 618 ou 645, mas por não haver desafios, não o abordaremos.
Toda vez que for mencionado “Nylon” nesta série de posts se referirá ao Nylon de roçadeira. O Nylon próprio para impressão 3D será mencionado como “Taulman”.

### Requisitos do Nylon
O Nylon precisa ser puro, ou seja 100% Poliamida. Existem marcas que adicionam fibra de vidro e sua composição, este não serve para nossos testes.
Precisa ser redondo. Existem marcas em que o filamento é quadrado ou em outras formas, este também não serve.
Diâmetro correto: Existe toda sorte de diâmetros mas como nossos hotends são geralmente 1.75 ou 3mm, tente se aproximar o máximo destes valores.

